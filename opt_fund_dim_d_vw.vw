DROP VIEW OPT_FUND_DIM_D_VW;
/* Formatted on 2/27/2018 4:20:36 PM (QP5 v5.185.11230.41888) */
CREATE OR REPLACE FORCE VIEW OPT_FUND_DIM_D_VW
(
   FUND_ID,
   ACCT_ID,
   BDF_RATE_NUM,
   BUS_UNIT_ID,
   CRNCY_TYPE_CODE,
   FN_END_DATE,
   FN_START_DATE,
   CREATED_ON,
   FUND_CLASS_CODE,
   FUND_GRP_CLASS,
   FUND_GRP_ID,
   FUND_GRP_NAME,
   FUND_NAME,
   FUND_TYPE_CODE,
   INTRN_ORDR_CODE,
   STTUS_CODE,
   USAGE_END_DATE,
   PARNT_FUND_SKID,
   FUND_CD,
   ROOF_FUND_GRP_SKID,
   ROOF_ORG_FUND_IND,
   ROOF_RATE_NUM,
   ACCT_FUND_PROD_IND,
   RET_DATE,
   RET_FUND_ID,
   SHPMT_TYPE_CODE,
   FRCST_MANUL_UPDT_IND,
   FRCST_FREEZ_IND,
   PARNT_FUND_ID,
   CENTL_BANK_IND,
   MDA_TYPE_IND,
   MPA_MANUL_UPDT_IND,
   FRCST_MODEL_NAME,
   FUND_ACTIV_IND
)
AS
   SELECT FA.ROW_ID AS FUND_ID,
          FA.ACCNT_ID AS ACCT_ID,
          DECODE (FA.MDF_TYPE_CD,
                  'Fixed', 0,
                  NVL (LKP_FUND_RATE.X_FUND_RATE, 0))
             AS BDF_RATE_NUM,
          FA.BU_ID AS BUS_UNIT_ID,
          FA.AMT_CURCY_CD AS CRNCY_TYPE_CODE,
          TO_CHAR (FA.MDF_END_DT, 'YYYYMMDDHH24MISS') AS FN_END_DATE,
          TO_CHAR (FA.MDF_START_DT, 'YYYYMMDDHH24MISS') AS FN_START_DATE,
          TO_CHAR (FA.CREATED, 'YYYYMMDDHH24MISS') AS CREATED_ON,
          FA.X_FND_CLAS AS FUND_CLASS_CODE,
          FG.X_FND_CLAS AS FUND_GRP_CLASS,
          FG.ROW_ID AS FUND_GRP_ID,
          FG.NAME AS FUND_GRP_NAME,
          FA.NAME AS FUND_NAME,
          FA.MDF_TYPE_CD AS FUND_TYPE_CODE,
          DECODE (FA.X_INT_ORDER_1 || FA.FUND_CD,
                  'Account Fund',  /* If Account Fund and its order is null */
                                 FG.X_INT_ORDER_1,
                  FA.X_INT_ORDER_1)
             AS INTRN_ORDR_CODE,
          FA.STATUS_CD AS STTUS_CODE,
          DECODE (FA.X_USAGE_END_DT,
                  NULL, TO_CHAR (FG.X_USAGE_END_DT, 'YYYYMMDDHH24MISS'),
                  TO_CHAR (FA.X_USAGE_END_DT, 'YYYYMMDDHH24MISS'))
             AS USAGE_END_DATE,
          FA.PAR_MDF_ID AS PARNT_FUND_SKID,
          FA.FUND_CD AS FUND_CD,
          FG.X_ROOF_FUND_GRP_ID AS ROOF_FUND_GRP_SKID,
          NVL (TRIM (FG.X_ROOF_ORG_FLG), 'N') AS ROOF_ORG_FUND_IND,
          DECODE (FA.MDF_TYPE_CD,
                  'Fixed', 0,
                  NVL (LKP_FUND_RATE.X_RF_RATE, 0))
             AS ROOF_RATE_NUM,
          (CASE
              WHEN     FA.FUND_CD = 'Account Fund'
                   AND LKP_FUND_RATE.PAR_ROW_ID2 IS NOT NULL
              THEN
                 'Y'
              ELSE
                 'N'
           END)
             AS ACCT_FUND_PROD_IND,
          TO_CHAR (FA.X_RETURN_FUND_DT, 'YYYYMMDDHH24MISS') AS RET_DATE,
          FA.X_RETURN_FUND_ID AS RET_FUND_ID,
          ---- add 3 columns to Fund DIM for R10 B007 by Lou Yang --begin
          FG.X_SHIPMENT_TYPE AS SHPMT_TYPE_CODE,
          NVL (TRIM (FA.X_FCST_MANUAL_UPD_FLG), 'N') AS FRCST_MANUL_UPDT_IND,
          --Optima11, CR1161, Kathryn, May-4-2011, FRCST_FREEZ_IND should be at Fund Group level, Begin
          NVL (TRIM (FG.X_FREEZE_FLG), 'N') AS FRCST_FREEZ_IND,
          --Optima11, CR1161, Kathryn, May-4-2011, FRCST_FREEZ_IND should be at Fund Group level, End
          ---- add 3 columns to Fund DIM for R10 B007 by Lou Yang --end
          ---- add 3 columns to Fund DIM for R10 B034 by Eric
          ---- convert it to N when the column CENTL_BANK_IND is null for R10 B034 by Eric
          FA.PAR_MDF_ID AS PARNT_FUND_ID,
          NVL (FG.X_BANK_ACCNT_FLG, 'N') AS CENTL_BANK_IND,
          NVL (LST.X_WORKING_FLG, 'N') AS MDA_TYPE_IND,
          NVL (TRIM (FA.X_MPA_MANUAL_UPD_FLG), 'N') AS MPA_MANUL_UPDT_IND,
          --Optima11,CR872,3-Nov-2010,Yves Begin
          FA.X_FORECAST_LEVEL AS FRCST_MODEL_NAME,
          --Optima11,CR872,3-Nov-2010,Yves End
          --Optima11,CR822,2010-12-06,Kathryn,Add column FUND_ACTIV_IND, begin
          FA.SUMMARY_FLG AS FUND_ACTIV_IND
     --Optima11,CR822,2010-12-06,Kathryn,Add column FUND_ACTIV_IND, end
     FROM S_MDF FA,
          S_MDF FG,
          (  SELECT SUM (S_MDF_XM.X_FUND_RATE) X_FUND_RATE,
                    SUM (S_MDF_XM.X_RF_RATE) X_RF_RATE,
                    S_MDF_XM.PAR_ROW_ID,
                    --NEED BE CONSISTENT WITH OPT_FUND_PROD_FDIM EXTRACTION SQL
                    DECODE (SUM (DECODE (S_MDF_XM.TYPE, 'Accrual', 1, 0)),
                            0, NULL,
                            S_MDF_XM.PAR_ROW_ID)
                       AS PAR_ROW_ID2
               FROM S_MDF_XM
           GROUP BY S_MDF_XM.PAR_ROW_ID) LKP_FUND_RATE,
          OPT_BUS_UNIT_SKID_VW R,
          ---- add 3 columns to Fund DIM for R10 B007 by Lou Yang --begin
          H_TFM_OPT_FUND_MASTER HFM,
          --S_MDF_PLC PLC,
          ---- add 3 columns to Fund DIM for R10 B007 by Lou Yang --end
          ---- add 3 columns to Fund DIM for R10 B034 by Eric
          (  SELECT VAL, X_WORKING_FLG, BU_ID
               FROM S_LST_OF_VAL
              WHERE    CODE <> 'CLASS'
                    OR     CODE IS NULL
                       AND X_WORKING_FLG IS NOT NULL
                       AND VAL NOT LIKE '%BSA%'
           GROUP BY VAL, X_WORKING_FLG, BU_ID) LST
    WHERE     FG.ROW_ID(+) = FA.ROOT_MDF_ID
          AND FA.ROW_ID = LKP_FUND_RATE.PAR_ROW_ID(+)
          AND FA.FUND_CD IN ('Fund Group', 'Account Fund')
          AND FA.BU_ID = R.BUS_UNIT_ID
          ---- add 3 columns to Fund DIM for R10 B007 by Lou Yang --begin
          --AND FG.ROW_ID = PLC.ROW_ID(+)
          AND FA.ROW_ID = HFM.ACCNT_FUND_ID(+)
          AND FA.ACCNT_ID = HFM.ACCNT_ID(+)
          ---- add 3 columns to Fund DIM for R10 B007 by Lou Yang --end
          --Added by David o 2010-3-25
          AND FA.BU_ID = HFM.BU_ID(+)
          AND FA.BU_ID = FG.BU_ID(+)
          ---- add 3 columns to Fund DIM for R10 B007 by Lou Yang --end
          ---- add 3 columns to Fund DIM for R10 B034 by Eric
          AND FG.BU_ID = LST.BU_ID(+)
          AND FG.X_FND_CLAS = LST.VAL(+);


DROP SYNONYM ADWG_OPTIMA_APEX_ETL.OPT_FUND_DIM_D_VW;

CREATE OR REPLACE SYNONYM ADWG_OPTIMA_APEX_ETL.OPT_FUND_DIM_D_VW FOR OPT_FUND_DIM_D_VW;


GRANT SELECT ON OPT_FUND_DIM_D_VW TO ADWG_OPTIMA_SELECT;
