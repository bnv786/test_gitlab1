DROP VIEW OPT_PRMTN_DIM_D_VW;
/* Formatted on 2/27/2018 4:31:26 PM (QP5 v5.185.11230.41888) */
CREATE OR REPLACE FORCE VIEW OPT_PRMTN_DIM_D_VW
(
   PRMTN_ID,
   PRMTN_NAME,
   PRMTN_PERD_END_DATE,
   PRMTN_PERD_START_DATE,
   SHPMT_END_DATE,
   SHPMT_START_DATE,
   PRMTN_PLAN_ID,
   PRMTN_PLN_NAME,
   PRMTN_PLN_STTUS_CODE,
   PRMTN_PRICE_LIST_ID,
   PGM_START_DATE,
   PGM_END_DATE,
   PRMTN_STOP_DATE,
   PRMTN_STTUS_CODE,
   PRMTN_TYPE_CODE,
   REGN_ID,
   STAT_CODE,
   SUB_TYPE_CODE,
   TACTICS_CODE,
   ACCT_ID,
   APPROVER_ID,
   APPROVER_STTUS_CODE,
   CMMNT,
   CORP_PRMTN_NAME,
   CORP_PRMTN_TYPE_CODE,
   CTLG_CAT_ID,
   DATE_SKID,
   OWND_BY_NAME,
   PKG_DESC,
   PRMTN_UPDTD_BY_DESC,
   BUS_UNIT_ID,
   CREATED_DATE,
   CNFRM_DATE,
   APPRV_DATE,
   AUTO_UPDT_GTIN_IND,
   PRMTN_STOP_IND,
   RELTD_PRMTN_ID,
   CNBLN_PCT,
   CNBLN_WK_CNT,
   SHPMT_WK_0_PCT,
   SHPMT_WK_MINUS_1_PCT,
   SHPMT_WK_MINUS_2_PCT,
   SHPMT_WK_MINUS_3_PCT,
   SHPMT_WK_MINUS_4_PCT,
   SHPMT_WK_MINUS_5_PCT,
   SHPMT_WK_MINUS_6_PCT,
   SHPMT_WK_MINUS_7_PCT,
   SHPMT_WK_MINUS_8_PCT,
   SHPMT_WK_PLUS_1_PCT,
   SHPMT_WK_PLUS_2_PCT,
   SHPMT_WK_PLUS_3_PCT,
   SHPMT_WK_PLUS_4_PCT,
   SHPMT_WK_PLUS_5_PCT,
   SHPMT_WK_PLUS_6_PCT,
   SHPMT_WK_PLUS_7_PCT,
   SHPMT_WK_PLUS_8_PCT,
   SHPMT_WK_PLUS_9_PCT,
   SHPMT_WK_PLUS_10_PCT,
   SHPMT_WK_PLUS_11_PCT,
   SHPMT_WK_PLUS_12_PCT,
   SPLIT_BY_SHPMT_PATRN_IND,
   PAYT_ACCT_ID,
   CNTRT_NUM,
   CROSS_FY_IND,
   PYMT_INTV_NAME,
   PYMT_METHD_DESC,
   X_ST_MONTH_ID,
   X_END_MONTH_ID,
   X_SHIP_ST_MONTH_ID,
   X_SHIP_END_MONTH_ID,
   X_FORECAST_MODEL,
   X_ANAPLAN_ID
)
AS
   SELECT PROMO.ROW_ID AS PRMTN_ID,
          PROMO.NAME AS PRMTN_NAME,
          TO_CHAR (TRUNC (PROMO.CONSUME_END_DT, 'DD'), 'YYYYMMDDHH24MISS')
             AS PRMTN_PERD_END_DATE,
          TO_CHAR (TRUNC (PROMO.CONSUME_START_DT, 'DD'), 'YYYYMMDDHH24MISS')
             AS PRMTN_PERD_START_DATE,
          TO_CHAR (TRUNC (PROMO.SHIP_END_DT, 'DD'), 'YYYYMMDDHH24MISS')
             AS SHPMT_END_DATE,
          TO_CHAR (TRUNC (PROMO.SHIP_START_DT, 'DD'), 'YYYYMMDDHH24MISS')
             AS SHPMT_START_DATE,
          CS_PLAN.ROW_ID AS PRMTN_PLAN_ID,
          CS_PLAN.NAME AS PRMTN_PLN_NAME,
          CS_PLAN.STATUS_CD AS PRMTN_PLN_STTUS_CODE,
          PROMO.PROMO_PRI_LST_ID AS PRMTN_PRICE_LIST_ID,
          TO_CHAR (TRUNC (PROMO.PROG_START_DT, 'DD'), 'YYYYMMDDHH24MISS')
             AS PGM_START_DATE,
          TO_CHAR (TRUNC (PROMO.PROG_END_DT, 'DD'), 'YYYYMMDDHH24MISS')
             AS PGM_END_DATE,
          TO_CHAR (TRUNC (PROMO.X_STOPPED_DT, 'DD'), 'YYYYMMDDHH24MISS')
             AS PRMTN_STOP_DATE,
          PROMO.STATUS_CD AS PRMTN_STTUS_CODE,
          CORPO_PROMO.SRC_CD AS PRMTN_TYPE_CODE,
          PROMO.REGION_ID AS REGN_ID,
          PROMO.STAT_CD AS STAT_CODE,
          PROMO.SUB_TYPE AS SUB_TYPE_CODE,
          S_SRC_CHNL.TACTICS_CD AS TACTICS_CODE,
          PROMO.PR_ACCNT_ID AS ACCT_ID,
          PROMO.X_APPROVER_ID AS APPROVER_ID,
          PROMO.X_APPR_STATUS_CD AS APPROVER_STTUS_CODE,
          REPLACE (REPLACE (PROMO.COMMENTS, CHR (13), CHR (07)),
                   CHR (10),
                   CHR (06))
             AS CMMNT,
          CORPO_PROMO.NAME AS CORP_PRMTN_NAME,
          PROMO.SRC_CD AS CORP_PRMTN_TYPE_CODE,
          PROMO.CTLG_CAT_ID AS CTLG_CAT_ID,
          CS_PLAN.PERIOD_ID AS DATE_SKID,
          PROMO.X_OWNED_BY AS OWND_BY_NAME,
          PROMO.PACKAGE_DESC_TEXT AS PKG_DESC,
          PROMO.LAST_UPD_BY AS PRMTN_UPDTD_BY_DESC,
          PROMO.BU_ID AS BUS_UNIT_ID,
          TO_CHAR (TRUNC (PROMO.CREATED, 'DD'), 'YYYYMMDDHH24MISS')
             AS CREATED_DATE,
          TO_CHAR (TRUNC (PROMO.X_CONFIRM_DT, 'DD'), 'YYYYMMDDHH24MISS')
             AS CNFRM_DATE,
          TO_CHAR (TRUNC (PROMO.X_PROMO_APPRD_DATE, 'DD'),
                   'YYYYMMDDHH24MISS')
             AS APPRV_DATE,
          DECODE (NVL (LKP1.CNT, 0), 0, 'Y', 'N') AS AUTO_UPDT_GTIN_IND,
          CASE
             WHEN TRUNC (SYSDATE) >= PROMO.X_STOPPED_DT
             THEN
                'Y'
             WHEN     EXIST_CNT > 0
                  AND LIVE_CNT = 0
                  AND PROMO.STATUS_CD = 'Confirmed'
             THEN
                'Y'
             ELSE
                'N'
          END
             AS PRMTN_STOP_IND,
          PROMO.X_RELATED_PROMO_ID AS RELTD_PRMTN_ID,
          PROMO.X_PERCENTAGE_CANNIBALIZATION AS CNBLN_PCT,
          PROMO.X_CANNBZTN_WEEK AS CNBLN_WK_CNT,
          PROMO.X_SHIPMT_WEEK_0 AS SHPMT_WK_0_PCT,
          PROMO.X_SHIPMT_WEEK_MINUS_1 AS SHPMT_WK_MINUS_1_PCT,
          PROMO.X_SHIPMT_WEEK_MINUS_2 AS SHPMT_WK_MINUS_2_PCT,
          PROMO.X_SHIPMT_WEEK_MINUS_3 AS SHPMT_WK_MINUS_3_PCT,
          PROMO.X_SHIPMT_WEEK_MINUS_4 AS SHPMT_WK_MINUS_4_PCT,
          PROMO.X_SHIPMT_WEEK_MINUS_5 AS SHPMT_WK_MINUS_5_PCT,
          PROMO.X_SHIPMT_WEEK_MINUS_6 AS SHPMT_WK_MINUS_6_PCT,
          PROMO.X_SHIPMT_WEEK_MINUS_7 AS SHPMT_WK_MINUS_7_PCT,
          PROMO.X_SHIPMT_WEEK_MINUS_8 AS SHPMT_WK_MINUS_8_PCT,
          PROMO.X_SHIPMT_WEEK_PLUS_1 AS SHPMT_WK_PLUS_1_PCT,
          PROMO.X_SHIPMT_WEEK_PLUS_2 AS SHPMT_WK_PLUS_2_PCT,
          ---- Add 10 new columns for Extended Shipment Pattern Change in B024 of Optima10 by Staven
          PROMO.X_SHIPMT_WEEK_PLUS_3 AS SHPMT_WK_PLUS_3_PCT,
          PROMO.X_SHIPMT_WEEK_PLUS_4 AS SHPMT_WK_PLUS_4_PCT,
          PROMO.X_SHIPMT_WEEK_PLUS_5 AS SHPMT_WK_PLUS_5_PCT,
          PROMO.X_SHIPMT_WEEK_PLUS_6 AS SHPMT_WK_PLUS_6_PCT,
          PROMO.X_SHIPMT_WEEK_PLUS_7 AS SHPMT_WK_PLUS_7_PCT,
          PROMO.X_SHIPMT_WEEK_PLUS_8 AS SHPMT_WK_PLUS_8_PCT,
          PROMO.X_SHIPMT_WEEK_PLUS_9 AS SHPMT_WK_PLUS_9_PCT,
          PROMO.X_SHIPMT_WEEK_PLUS_10 AS SHPMT_WK_PLUS_10_PCT,
          PROMO.X_SHIPMT_WEEK_PLUS_11 AS SHPMT_WK_PLUS_11_PCT,
          PROMO.X_SHIPMT_WEEK_PLUS_12 AS SHPMT_WK_PLUS_12_PCT,
          CASE
             WHEN                               --Plan Pattern Id is populated
                  (    (   (PROMO.X_PLAN_SHIPMENT_PATTERN_ID IS NOT NULL)
                        OR --Plan Pattern Id is not populated and any of Shipment Pattern is populated
                           (    PROMO.X_PLAN_SHIPMENT_PATTERN_ID IS NULL
                            AND (   NVL (PROMO.X_SHIPMT_WEEK_MINUS_8, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_MINUS_7, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_MINUS_6, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_MINUS_5, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_MINUS_4, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_MINUS_3, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_MINUS_2, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_MINUS_1, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_0, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_PLUS_1, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_PLUS_2, 0) <> 0
                                 OR ---- Add 10 new columns for Extended Shipment Pattern Change in B024 of Optima10 by Staven
                                   NVL (PROMO.X_SHIPMT_WEEK_PLUS_3, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_PLUS_4, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_PLUS_5, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_PLUS_6, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_PLUS_7, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_PLUS_8, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_PLUS_9, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_PLUS_10, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_PLUS_11, 0) <> 0
                                 OR NVL (PROMO.X_SHIPMT_WEEK_PLUS_12, 0) <> 0)))
                   AND --Promotion length <= 21 weeks                                                                    ----- Changed by Staven for Optima10 B024 Req. on Jan-20, 2010  (11-->21)
                       ( (PROMO.PROG_END_DT - PROMO.PROG_START_DT + 1) <= 147))
             THEN ----- Changed by Staven for Optima10 B024 Req. on Jan-20, 2010  (11-->21)
                'Y'
             ELSE
                'N'
          END
             AS SPLIT_BY_SHPMT_PATRN_IND,
          PROMO.X_PAY_TO_ID AS PAYT_ACCT_ID,
          PROMO.X_CONTRACT_ID AS CNTRT_NUM,
          NVL (PROMO.X_CROSS_FY_FLG, 'N') AS CROSS_FY_IND,
          PROMO.X_ACTIVITY_PYMNT_FREQ AS PYMT_INTV_NAME,
          LKP_PRMTN_STOP_IND.X_PROMO_PAYMENT_MEANS AS PYMT_METHD_DESC, --R9 UAT defect CR2588 change for payment method
          PROMO.X_ST_MONTH_ID,
          PROMO.X_END_MONTH_ID,
          PROMO.X_SHIP_ST_MONTH_ID,
          PROMO.X_SHIP_END_MONTH_ID,
          --Optima11,CR872,5-Nov-2010, Gary, Begin
          PROMO.X_FORECAST_MODEL,
          --Optima11,CR872,5-Nov-2010, Gary, End
          PROMO.X_ANAPLAN_ID -- Added this column for UKTPM Merger Project -03Nov2017 - Venkatesh
     FROM S_SRC PROMO,
          S_SRC CS_PLAN,
          S_SRC CORPO_PROMO,
          S_SRC_CHNL,
          OPT_BUS_UNIT_SKID_VW R,
          (  SELECT CATEG.PAR_SRC_ID, COUNT (1) AS CNT
               FROM S_SRC CATEG
              WHERE     CATEG.SUB_TYPE = 'PLAN_ACCT_PROMOTION_CATEGORY'
                    AND (   CATEG.X_AUTO_GTIN_FLG = 'N'
                         OR CATEG.X_AUTO_GTIN_FLG IS NULL)
           GROUP BY CATEG.PAR_SRC_ID) LKP1,
          (  SELECT ACTVY.PAR_SRC_ID,
                    COUNT (1) AS EXIST_CNT,
                    COUNT (
                       CASE
                          WHEN    ACTVY.X_STOPPED_DT IS NULL
                               OR TRUNC (SYSDATE) < ACTVY.X_STOPPED_DT
                          THEN
                             1
                          ELSE
                             NULL
                       END)
                       AS LIVE_CNT,
                    OPT_STRING_AGG (ACTVY.X_PROMO_PAYMENT_MEANS)
                       AS X_PROMO_PAYMENT_MEANS --UAT defect CR2588 change for payment method
               FROM S_SRC ACTVY
              WHERE ACTVY.SUB_TYPE = 'PG_TFMAAI_PROMOTION_ACTIVITY'
           GROUP BY ACTVY.PAR_SRC_ID) LKP_PRMTN_STOP_IND
    WHERE     PROMO.PAR_SRC_ID = CS_PLAN.ROW_ID(+)
          AND PROMO.TMPL_ID = CORPO_PROMO.ROW_ID(+)
          AND PROMO.ROW_ID = S_SRC_CHNL.PAR_ROW_ID(+)
          AND PROMO.SUB_TYPE = 'PLAN_ACCOUNT_PROMOTION'
          AND CORPO_PROMO.SUB_TYPE(+) = 'PROMOTION'
          AND CS_PLAN.SUB_TYPE(+) = 'PLAN_ACCOUNT'
          AND PROMO.STATUS_CD <> 'Pending'
          AND PROMO.BU_ID = R.BUS_UNIT_ID
          AND PROMO.ROW_ID = LKP1.PAR_SRC_ID(+)
          AND PROMO.ROW_ID = LKP_PRMTN_STOP_IND.PAR_SRC_ID(+);


DROP SYNONYM ADWG_OPTIMA_APEX_ETL.OPT_PRMTN_DIM_D_VW;

CREATE OR REPLACE SYNONYM ADWG_OPTIMA_APEX_ETL.OPT_PRMTN_DIM_D_VW FOR OPT_PRMTN_DIM_D_VW;


GRANT SELECT ON OPT_PRMTN_DIM_D_VW TO ADWG_OPTIMA_SELECT;
