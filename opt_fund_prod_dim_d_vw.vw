DROP VIEW OPT_FUND_PROD_DIM_D_VW;
/* Formatted on 2/27/2018 4:23:20 PM (QP5 v5.185.11230.41888) */
CREATE OR REPLACE FORCE VIEW OPT_FUND_PROD_DIM_D_VW
(
   FUND_PROD_SKID,
   FUND_TRGT_FLG,
   BDF_FUND_RATE_NUM,
   FUND_GRP_ID,
   PROD_ID,
   PROD_NAME,
   PROD_DESC,
   PROD_LVL_DESC,
   PROD_START_DATE,
   PROD_END_DATE,
   BUS_UNIT_SKID,
   ROOF_RATE_NUM,
   OVLAP_IND,
   CMMNT_DESC,
   FUND_RATE_DESC
)
AS
   SELECT B.ROW_ID AS FUND_PROD_SKID,
          'F' AS FUND_TRGT_FLG,
          B.X_FUND_RATE AS BDF_FUND_RATE_NUM,
          B.PAR_ROW_ID AS FUND_GRP_ID,
          B.X_PRDINT_ID AS PROD_ID,
          A.NAME AS PROD_NAME,
          DECODE (A.X_UI_PROD_LEVEL_DESC,
                  'GTIN', NVL (C.DESC_TEXT, A.DESC_TEXT),
                  A.DESC_TEXT)
             AS PROD_DESC,
          A.X_UI_PROD_LEVEL_DESC AS PROD_LVL_DESC,
          TO_CHAR (B.X_START_DT, 'YYYYMMDDHH24MISS') AS PROD_START_DATE,
          TO_CHAR (B.X_END_DT, 'YYYYMMDDHH24MISS') AS PROD_END_DATE,
          A.BU_ID AS BUS_UNIT_SKID,
          B.X_RF_RATE AS ROOF_RATE_NUM,
          B.X_OVERLAP_FLG AS OVLAP_IND,
          B.X_COMMENT AS CMMNT_DESC,
          NULL AS FUND_RATE_DESC -- Added for UKTPM Merger Project -03Nov2017- venkatesh
     FROM S_MDF_XM B,
          S_PROD_INT A,
          S_PROD_INT_LANG C,
          OPT_BUS_UNIT_SKID_VW R
    WHERE     B.X_PRDINT_ID = A.ROW_ID(+)
          AND B.X_PRDINT_ID = C.PAR_ROW_ID(+)
          AND (C.LANG_ID = 'ENU' OR C.LANG_ID IS NULL)
          AND B.TYPE = 'Accrual'
          AND A.BU_ID = R.BUS_UNIT_ID
   UNION ALL
   SELECT DISTINCT
          B.ROW_ID AS FUND_PROD_SKID,
          'T' AS FUND_TRGT_FLG,
          NULL AS BDF_FUND_RATE_NUM,
          --If the Fund is Account Fund and PAR_ROW_ID2 is not null(same as ACCT_FUND_PROD_IND = 'Y' in Fund Dim),
          --  FUND_GRP_SKID will be the same as FUND_SKID by Mervyn At 2010-04-21 Optima 10 CR3272
          (CASE
              WHEN     FA.FUND_CD = 'Account Fund'
                   AND LKP_FUND_RATE.PAR_ROW_ID2 IS NOT NULL
              THEN
                 FA.ROW_ID
              ELSE
                 B.MDF_ID
           END)
             AS FUND_GRP_ID,
          B.PROD_INT_ID AS PROD_ID,
          A.NAME AS PROD_NAME,
          DECODE (A.X_UI_PROD_LEVEL_DESC,
                  'GTIN', NVL (C.DESC_TEXT, A.DESC_TEXT),
                  A.DESC_TEXT)
             AS PROD_DESC,
          A.X_UI_PROD_LEVEL_DESC AS PROD_LVL_DESC,
          TO_CHAR (B.TGT_PROD_START_DT, 'YYYYMMDDHH24MISS')
             AS PROD_START_DATE,
          TO_CHAR (B.TGT_PROD_END_DT, 'YYYYMMDDHH24MISS') AS PROD_END_DATE,
          A.BU_ID AS BUS_UNIT_SKID,
          0 AS ROOF_RATE_NUM,
          'N' AS OVLAP_IND,
          NULL AS CMMNT_DESC,
          NULL AS FUND_RATE_DESC -- Added for UKTPM Merger Project -03Nov2017- venkatesh
     FROM S_MDF_TGT_PROD B,
          S_PROD_INT A,
          S_PROD_INT_LANG C,
          --Add Fund Group Logic following Fund Dimension by Mervyn At 2010-04-21 Optima 10 CR3272
          S_MDF FA,
          S_MDF FG,
          (  SELECT SUM (S_MDF_XM.X_FUND_RATE) X_FUND_RATE,
                    SUM (S_MDF_XM.X_RF_RATE) X_RF_RATE,
                    S_MDF_XM.PAR_ROW_ID,
                    --NEED BE CONSISTENT WITH OPT_FUND_PROD_FDIM EXTRACTION SQL
                    DECODE (SUM (DECODE (S_MDF_XM.TYPE, 'Accrual', 1, 0)),
                            0, NULL,
                            S_MDF_XM.PAR_ROW_ID)
                       AS PAR_ROW_ID2
               FROM S_MDF_XM
           GROUP BY S_MDF_XM.PAR_ROW_ID) LKP_FUND_RATE,
          OPT_BUS_UNIT_SKID_VW R
    WHERE     A.ROW_ID = B.PROD_INT_ID
          AND A.ROW_ID = C.PAR_ROW_ID(+)
          AND (C.LANG_ID = 'ENU' OR C.LANG_ID IS NULL)
          AND A.BU_ID = R.BUS_UNIT_ID
          AND FG.ROW_ID(+) = FA.ROOT_MDF_ID
          AND FA.ROW_ID = LKP_FUND_RATE.PAR_ROW_ID(+)
          AND FA.FUND_CD IN ('Fund Group', 'Account Fund')
          AND FA.BU_ID = R.BUS_UNIT_ID
          AND B.MDF_ID = FA.ROOT_MDF_ID
   UNION ALL
   SELECT FP.ROW_ID AS FUND_PROD_SKID,
          'R' AS FUND_TRGT_FLG,
          FP.X_FUND_RATE AS BDF_FUND_RATE_NUM,
          RF.ROW_ID AS FUND_GRP_ID,
          FP.X_PRDINT_ID AS PROD_ID,
          A.NAME AS PROD_NAME,
          DECODE (A.X_UI_PROD_LEVEL_DESC,
                  'GTIN', NVL (C.DESC_TEXT, A.DESC_TEXT),
                  A.DESC_TEXT)
             AS PROD_DESC,
          A.X_UI_PROD_LEVEL_DESC AS PROD_LVL_DESC,
          TO_CHAR (FP.X_START_DT, 'YYYYMMDDHH24MISS') AS PROD_START_DATE,
          TO_CHAR (FP.X_END_DT, 'YYYYMMDDHH24MISS') AS PROD_END_DATE,
          A.BU_ID AS BUS_UNIT_SKID,
          FP.X_RF_RATE AS ROOF_RATE_NUM,
          FP.X_OVERLAP_FLG AS OVLAP_IND,
          FP.X_COMMENT AS CMMNT_DESC,
          NULL AS FUND_RATE_DESC -- Added for UKTPM Merger Project -03Nov2017- venkatesh
     FROM S_MDF RF,
          S_MDF FG,
          S_MDF_XM FP,
          S_PROD_INT A,
          S_PROD_INT_LANG C,
          OPT_BUS_UNIT_SKID_VW R
    WHERE     RF.X_ROOF_ORG_FLG = 'Y'
          AND FP.X_PRDINT_ID = A.ROW_ID(+)
          AND FP.X_PRDINT_ID = C.PAR_ROW_ID(+)
          AND (C.LANG_ID = 'ENU' OR C.LANG_ID IS NULL)
          AND RF.ROW_ID = FG.X_ROOF_FUND_GRP_ID
          AND FG.ROW_ID = FP.PAR_ROW_ID
          AND RF.FUND_CD = 'Fund Group'
          AND FG.FUND_CD = 'Fund Group'
          AND FP.TYPE = 'Accrual'
          AND A.BU_ID = R.BUS_UNIT_ID;


DROP SYNONYM ADWG_OPTIMA_APEX_ETL.OPT_FUND_PROD_DIM_D_VW;

CREATE OR REPLACE SYNONYM ADWG_OPTIMA_APEX_ETL.OPT_FUND_PROD_DIM_D_VW FOR OPT_FUND_PROD_DIM_D_VW;


GRANT SELECT ON OPT_FUND_PROD_DIM_D_VW TO ADWG_OPTIMA_SELECT;
